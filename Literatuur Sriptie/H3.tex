\section{ Product Configurators: Introduction}

Product Configurators are an essential element in the BTO model -
to achieve rapid, accurate configurations of unique products.
Before Product Configurators and modular design concepts, the most
common approach to unique product definition was to assign part
numbers and create a BoM for every conceivable end-item. The
results were often massive, unwieldy product databases requiring
extraordinary maintenance efforts - and riddled with errors. One
essential issue, therefore, was how to define options and
variations for unique product configurations without creating such
problems.

The answer - Product Configurators. A general definition is - -
software modules with logic capabilities to create, maintain, and
use electronic product models that allow complete definition of
all possible product option and variation combinations, with a
minimum of data entries and maintenance. When effectively
deployed, Product Configurators are used throughout the product
definition activities in a product's life cycle: design and
development, Sales Force Automation, and manufacturing -including
the supply chain of customers and suppliers. Product
Configurators: Design and Development

There are numerous approaches to Product Configurators, including
using the configurator logic in Product Data Management (PDM)
software, such as MatrixOne and Motiva. As viewed by Wayne
Collier, of D. H. Brown Associates, Product Configurators are a
design and development tool, rather than a redocumentation tool
for product configuration in ERP systems. This approach to product
development for configurable products is called �Generic Product
Structures.� The essence is for design engineers to model the
product and option selection rules explicitly to identify and
validate the product features and variants within a generic
(master) structure. The objective is to completely model the
product, including all variety, and validate the Bill of Material
(BoM) before release to manufacturing. In this approach, design
intelligence is captured from the start of product development; in
other words, no more "throw the design prints over the wall"
syndrome.

To gain the full benefits of Product Configurators, companies must
also consider the following essential activities within the scope
of product design and development.

Modular Design - the concept of building smaller sub-systems,
designed independently, and able to function properly when
assembled and tested as an end-item. Parts Standardization -
reducing duplicate (or more) parts to increase flexibility, reduce
costs, and encourage design reuse of preferred parts.

For some companies, an additional process may be mandatory -
Product Line Rationalization (PLR). This process becomes critical
when a company has conditions such as bloated and unprofitable
product lines, and a mish-mash of features and options. In other
words, loading such information into a Product Configurator may
only lead to disappointing results. PLR is a management-guided
process facilitated by the capabilities of Component \& Supplier
Management (CSM) and Design for Manufacturing/Assembly (DFM/A)
software, and using activity, cost, and volume information from
ERP systems. For best results, the PLR effort must be a
cooperative multi-discipline approach involving engineering,
marketing, manufacturing, and finance. Product Configurators:
Sales Force Automation

Sales Force Automation (SFA) is the use of advanced IT to automate
the total sales cycle to gain a competitive advantage by more
accurate configuring, quoting, and order entry. SFA systems
consist of many sub-systems, such as Quotation and Proposal
Preparation, Sales Analysis and the essential sub-system for mass
customization - the Product Configurator module. Once developed
and deployed, the module allows the rapid definition of unique,
often complex product configurations.

The capability of this module, and how effectively a company uses
it, determines whether a company gains an important benefit -
accurate order entry of a customized product configuration without
constant, further validation editing by engineering personnel.

The product model can be used in the SFA process in three basic
methods, which are:

    * Mobile lap top computers�for use by sales personnel on the customer's site for assisted buying
    * Web-based at the buyer's site�for quoting, configuring, and order entry�without assistance from an on-site sales representative
    * Computers manned by inside sales personnel at the company's manufacturing or headquarters site.

Any of the three methods can be used in the BTO business model
depending on the product complexity and/or the sophistication of
the customer buyer. The first alternative is more appropriate for
complex products�or with less sophisticated buyers. The opposite
is valid for the Web-based method. The third method is rapidly
being replaced by the first two methods. The final output of the
SFA process to manufacturing is a unique product configuration
commonly referred to as the "order BoM." Product Configurators:
Manufacturing/Assembly

The "order BoM" is input to an ERP system for completion of the
order fulfillment cycle in a lean (agile) manufacturing
environment geared to support the BTO strategy. Moreover, once an
order for a customized product is confirmed, the BTO business
model also relies heavily on the capabilities of PDM systems to
rapidly provide unique product and process information to the
plant floor - and to suppliers in a B2B environment. For example,
manufacturing and assembly work instructions can be created from
the customized product configuration generated at customer order
entry time - often by use of parametric drawings and animation
graphics.

In addition to Product Configurators, successful mass
customization demands the ability of the manufacturing
organization - including the supply chain - to be highly flexible
and responsive to customer demands. Manufacturing processes may
need to be revised extensively, and software, such as Advanced
Planning \& Scheduling (APS) implemented. APS functionality -
coupled with an agile manufacturing organization - promises the
shortest possible order fulfillment cycle time and vastly improves
customer delivery performance. Order fulfillment cycle times can
be driven down to the point where a company can operate
effectively without a forecast�the �purest" form of the BTO model.
Product Configurators: Sources of Software

In the past, simplistic Product Configurators were often provided
as a module in ERP systems. As product configurations have become
more complex, however, numerous sophisticated software methods
have evolved. As mentioned above, there are now various approaches
and methodologies from a variety of sources.

Product Configurator modules (logic) are currently provided by
most major PDM vendors, many ERP vendors, some SFA and Customer
Relationship Management (CRM) vendors, and a few third party
vendors, such as ILOG and Configuration Systems and Consulting.
One caution point in software selection, however, is that the
logic in PDM systems may not always be adequate to handle the
"front-end" needs of SFA, such as those for complex pricing
schemes. Another consideration when evaluating software
alternatives is whether spatial and dimensional needs in the
configuration process can be met. Only a few vendors provide
software capable of the full range of needed capabilities spanning
quoting and sales through customized manufacturing instructions.

To provide a breadth of configurator capabilities from one vendor
source, acquisitions and alliances often are the answer. For
example, J.D. Edwards acquired The Premisys Corporation. The
latter firm provided sales engineering automation software called
CustomWorks to manufacturers of complex, configured products
requiring custom engineering with spatial and dimensional design.
One version of the Premisys product runs with AutoCAD to provide
spatial layout capabilities. Integrated with J.D. Edwards OneWorld
ERP, the combined product offers to BTO companies both parametric
capabilities and modular options/variations selection. The
"front-office " SFA capability can generate technical documents
needed for initial quotation activity and customer design review
before order entry.

Another example is the strategic alliance of QAD, developer of
e-business-enabled enterprise and supply chain software, and
ACCESS Commerce, developers of Cameleon, a Product Configurator to
address the specialized needs of the BTO sector. The Cameleon
product is embedded in QAD's MFG/PRO software providing seamless
access, display, and use of product configuration information for
users of the two systems. (See the sidebar for a description of
the systems at Uniloy Milacron.) Systems Planning and
Implementation

The myriad of sources for configurator logic places a premium on a
thoughtful selection and evaluation process. Moreover, when
planning for a BTO model, consider that today's leading edge
advances will be tomorrow's competitive standards. Some of the
latest advances in IT capabilities are:

More configurator logic in CAD, CRM, and PDM software - less in
ERP systems. Integration of Product Configurators with APS
functionality. Customer Relationship Management software vendors
acquiring Product Configurator vendors. Conclusions

A company's vision of a �pure� BTO model - the epitome of mass
customization - is now within the realm of reality. Successful
companies will continue to transition from a �Make-to-Stock� to a
�Build-to-Order� environment - and to strive for the "pure" BTO
model. They will effectively link engineering, sales, marketing,
and manufacturing into a flexible, seamless system using a
"best-of-breed approach" that will focus on achieving the
substantial benefits of mass customization.
