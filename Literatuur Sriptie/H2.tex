\chapter{Mass Customisation}

``Producing goods and services to meet individual customer�s needs
with near mass production efficiency and prices'' this is the
definition of Mass Customisation. This seems a contradiction though.
In the next chapter the concept of mass customisation will be
explained trough the concepts of product families, variation and
custom-order-entry-point. Also a brief overview of industry types
will be provided as well as the application area.

\input{section1}


\input{CustDegr}

\section{Design for Mass Customisation}\label{sec:DFMC}

A mass customised product does not appear over-night. After
determining the type or approach to the customisation as explained
in section \ref{sec:CustApp} the way the product is conceived has to
be revised. \cite{Tseng1996} call this  Design for Mass
Customisation (DFMC). The assumption here is that a new product is
created. When an existing product is customised it should be treated
as if it were a new product. This will become clear when modularity
and product families are introduced and discussed ( sections
\ref{sec:mod} and \ref{sec:ProdFam})

\begin{wrapfigure}{R}{10cm}
\centering
  \includegraphics[width=.55\textwidth]{pictures/DFMC}\\
  \caption{Design for Mass Customisation \cite{Tseng2001}}\label{fig:DFMC}
\end{wrapfigure}

DFMC aims to consider economies of scope and scale (see section
\ref{sec:industry}) in the early product design stage, together with
the total design concept and in a concurrent engineering manner. The
main emphasis of DFMC is to explore how to set up a rational product
family architecture in order to conduct family-based design, rather
than design only a single product. The customisation can be achieved
effectively if the customers� requirements are characterised and
captured. Thus, the span of the design process has to be expanded
from the traditional design and manufacturing integration to include
sales and services functions.

Therefore, there are two basic concepts underpinning DFMC: product
family architecture (PFA) and product family design. Figure
\ref{fig:DFMC} summaries the conceptual implications of DFMC in
terms of the expansion of context from both a design scope
perspective and a product-differentiation perspective. Both PFA and
PFD will be discussed in more detail in section \ref{sec:ProdFam}


Mass customising starts at the very beginning of the product
conception. So to make a product or multiple products fit for mass
customisation starts by the way products are set up. DFMC
 is the practice to design in product families
rather than in individual products.\\ DFMC demands a larger scope in
the production perception, also marketing, distribution, services
and sales need to be involved in the process. They also play an
integrate part in the grand scheme.


\section{Industry Typologies}\label{sec:industry}

As seen earlier, the industry type plays a important role in the
whole customisation process. Both in the customisation continuum
 and the typologies of sections \ref{sec:CustApp} and
 \ref{sec:lampmintz} the volume of the products produced has
 influence on the decision to take a certain path.

In early years only high volume production industry (mass
production) could benefit from mass customisation now more and more
insight has been gained and a understanding has grown that also mid
and low volume industries can benefit from mass customisation. As
\cite{Tseng2001} show, if each individual customer's requirements
can be satisfied, customers are willing to pay premium prices for
this satisfaction, thus giving companies an additional bonus. Figure
\ref{fig:product volumes} shows both economies of scope and
economies of scale have an even amount of potential in mass
customisation. With the increasing flexibility built into modern
manufacturing systems and programmability in computing and
communication technologies, companies with low to medium production
volumes can gain an edge over competitors by implementing mass
customisation.\\ There is no evidence suggesting a certain industry
should not be able to benefit from mass customisation. The way the
customisation is handles can be very diverse though.


\begin{figure}[h]
\centering
  \includegraphics[width=.75\textwidth]{pictures/fig1.eps}\\
  \caption{Product volumes and mass customisation. \cite{Tseng2001}}\label{fig:product volumes}
\end{figure}


\subsection{Mass Industries}
Industries,   which make products such as disposable diapers and
gasoline, apply the logic of aggregation fairly consistently.
Production takes place in highly mechanised, inflexible facilities;
the products are standardised, and, indeed, often regarded as
commodities; and mass advertising is combined with mass distribution
to target customers through retail or wholesale organisations, so
that transactions tend to be generic. Pure standardisation thus
tends to be the most common, consistent strategy, although segmented
standardisation may become more common as the industry matures


\subsection{Thin Industries}
At the opposite  extreme are what we can call ``thin'' industries,
where customisation is pervasive, as in the production of
supercomputers and steam turbines. Products in these industries are
unique and are produced in craft settings. Transactions are
intermittent and highly individualised; they can be very large,
complex, and protracted, necessitating considerable cooperative
buyer and seller efforts. Buyers are closely involved in the design
of the product and generally expect a high commitment to after sales
service

\subsection{Catalog Industries}
In some industries,  the proliferation of designs favours the
strategies of segmented standardisation. Accordingly, firms tend to
organise their products and distribution on the basis of catalogs,
as in the record, book, toy, and pharmaceutical industries. The
buyer has a wide array of choices, although the transaction tends to
be generic, confined to making a selection. Sometimes the short
production runs can render the production process somewhat craft
like in nature, but often this is standardised mass production. The
products themselves, while certainly not unique, cannot be
considered commodities either. Product strategies, therefore, tend
to be segmented standardisation.

\subsection{Menu Industries}
In what we call menu industries, which produce products such as
printed circuit boards and financial services, buyers have a menu of
choices from which to select features of the final product. Thus
customised standardisation tends to be the preferred strategy.
Transactions involve negotiations and reciprocal relationships
between buyers and sellers. Once the configuration has been decided,
the production function assembles prefabricated components into
finished products. Thus, while transactions in menu industries
achieve a degree of customisation, fabrication, if not assembly,
takes place in dedicated, relatively inflexible facilities

\subsection{Tailoring Industries}
Individualisation is an important factor in tailoring industries,
such as residential housing and mainframe computers, where a
relatively standardised core design is adapted to individual
customer needs. Sellers personalise transactions to allow for
considerable customer input, mostly about peripheral design changes
and perhaps price, delivery conditions, and after-sale services. A
craft assembly system modifies the standard core design into a
dedicated product. As its name indicates, the dominant strategy in
these industries is tailored customisation, although the transaction
itself is customised. Shifts toward pure customisation can, however,
take place during periods of technological discontinuity or when
sellers deal with exceptionally powerful buyers

\subsection{Routing Industries}
Routing industries, such as data transmission and delivery services,
offer an intriguing mixture of standardisation and customisation.
They accept their customers' orders in purely generic ways but then
route them completely individually, in processes that are fixed and
inflexible! Thus a customer of the post office writes an address on
a letter and simply drops it in a mailbox. The transaction is
totally impersonal. But this standardised interface produces a
rather customised service; no two letters take the same route on the
same day (or else they would logically be combined in a single
envelope). The post office delivers 100 mi1lion letters on a
particular day that have followed 100 million different routes. But
while the service is customised, the process can be described as
customised standardisation, because the post office configures
standardised processes for each order. Put differently; the letters
are always handled in bulk even though each gets batched and
rebatched individually. In some routing industries, however,
tailored customisation can also be widespread. Powerful customers of
various courier businesses, for example, can demand special services
in addition to those normally offered

\subsection{Agent Industries}
Aggregation and individualisation also combine in professional
services such as health care and auditing. We call these agent
industries because sellers act as agents on behalf of buyers,
custodians of their financial, technical, or medical interests. The
transaction tends to be rather generic or standardised, governed by
standard contracts, and stipulated by professional or technical
codes of conduct. (We do not generally bargain over price with our
doctors.) The seller is ordinarily far more knowledgeable than the
buyer, which limits the latter's input into the operation's
processes. Yet these professional activities tend to be craft like
in nature, tailoring highly developed (meaning standardised) sets of
professional skills to specific customers' requirements. Processes
as well as the services themselves are therefore best described as
tailored customisation. In health care, for example, each
intervention (such as an appendix operation or a drug prescription
for a nervous disorder) is based on a standardised procedure adapted
to a particular patient's condition. Of course, complex
interventions (such as the treatment of a kidney transplant patient
with a potential stroke condition) can require such complicated
combinations of procedures that they begin to look like pure
customisation. But, by the same token, highly routine interventions,
such as certain cataract operations, can  seem like pure
standardisation

\subsection{Bulk Industries}
Bulk industries like nonferrous metals and coal produce large
volumes of standardised products that are sold (in bulk) to
customers, usually on long-term contracts. Thus, while the products
are commodities and the production facilities are automated and
relatively inflexible, the transactions tend to be personalised.
Sellers and buyers negotiate size of order, delivery conditions, and
price. Hence the strategies are essentially ones of pure
standardisation in product and process, yet pure customisation in
transaction.
\input{table5}


In table \ref{tab:lampmintz} the different industry classifications
as used by \cite{Lampel1996} are given according to the SIC codes.






\section{Product families}\label{sec:ProdFam}
Continuing on the principle of DFMC, product families are an
important tool to facilitate mass customisation. Creating a product
family is one of the first steps to the possibilities customisation
can offer.

To get an idea what product families mean one can look at the
product line at Airbus, they present a good example of product
families.

\begin{figure}[h]
  \centering
  \includegraphics[width=.65\textwidth]{pictures/airbus}\\
  \caption{ Four aircraft with the same flight crew, flight deck and cabin
  cross-section \cite{airbus2005}
}\label{fig:airbus}
\end{figure}


\begin{wrapfigure}{R}{9.5cm}
   \centering
  \includegraphics[width=.4\textwidth]{pictures/ProdFam}\\
  \caption{The Product Family The Product Core and Market Niche Applications}\label{fig:prodFam}
\end{wrapfigure}

In figure \ref{fig:airbus} one can see the product family of the
A318-321 aircraft family. They have the same cockpit,
fuselage-cross-section and cabin. This means parts can be exchanged
between aircraft form the same family, both with production and
maintenance. Secondly crews can be exchanged between aircraft, both
pilots and cabin crew with minimal extra training. This give the
customer (airline) extra flexibility in operating his aircraft.
Where as opposed to Boeing have individual aircraft such as the 737
the 747 and 777 are completely different aircraft with limited
interchange possibilities.

So from a manufacturer's point of view  a product family is a base
product from which each individual product can be obtained through a
configuration process. (See also \cite{Jorgensen2003})\\
\cite{Meyer1993} say: A product family can be defined as the
products that share a common platform but have specific features and
functionality required by different sets of customers. So the vital
part of a product family is the platform upon which the various
different derivatives are designed.  All the subsystems and
interfaces are all common to all products in the family. This
platform is also known as the \emph{product core}. As can be seen in
figure \ref{fig:prodFam} the product family targets the market
segment while the individual products the market niches address.
This leads to efficient and effectiveness in manufacturing,
distribution and service. \\ A good product family has two important
parts: design and implementation of design \cite{Meyer1993}. The
first is responsible for creating the \emph{product core}, standard
components and norms,  the latter is responsible for the final
product variations.

The variation is achieved by selection different predefined building
blocks also known as modules, in paragraph \ref{sec:mod} the theory
of modularity will be further evaluated. The product family concept
is made possible by using this modularity.

In the light of product families one has to consider also Product
family Design.

\subsection{Product family Design}
Product family design manifests itself through the derivation
processes of product variants based on (product family) PF
constructs. Customers make their selections among sets of options
defined for certain distinctive functional features. These
distinctive features are the differentiation enablers of PF in the
sales view. Selection constraints are defined for presenting
customers only feasible options, that is, both technically
affordable and market-wanted combinations. A set of selected
distinctive features together with those common features required by
all customers comprise the customer requirements of a customised
product design.\cite{Tseng2001}


\section{Modularity and Commonality } \label{sec:mod}

In order to create a product family a base is needed to build the
product on, this base is commonality and modularity. Company�s
internal operations are mainly driven by the level of common parts
and modules among a product family. When a lot of common modules are
used among many products, inventories can be reduced and companies
can achieve economies of scale.

To allow for the same component(s) to be used in multiple product
variants, and when feasible, in all product variants means a
reduction of speciality parts. In addition to the fact that,
modularity in product design may allow for the design of a loosely
coupled production system in which different subassemblies can be
made independently, and then rapidly assembled together in different
ways, given technical constraints, to build the final product
configurations. One important consequence of this increased
flexibility in the allocation of manufacturing tasks, is that firms
can more effectively pursue a postponement strategy.


What modularity means, is best viewed when setting side by side a
modular figure \ref{fig:ModArch} and an integral architecture figure
\ref{fig:IntArch}. Where with the modular architecture the mapping
of the functional components with the physical components is
one-on-one, the integral architecture is non one-on-one and much
more complex.

\noindent Through literature modularity is defined as:

A modular approach can reduce the variety of components while
offering a greater range of end products. Modularity allows part of
the product to be made in volume as standard modules with product
distinctiveness achieved through combination or modification of the
modules. Modularity provides both economies of scale and economies
of scope.\cite{Duray2002}

\begin{figure}[t]
 \centering
 \begin{minipage}[t]{0.45\linewidth}
    \includegraphics[width=\textwidth]{pictures/ModArch}\\
    \caption{Modular Architecture}\label{fig:ModArch}
  \end{minipage}
  \begin{minipage}[t]{0.45\linewidth}
     \includegraphics[width=\textwidth]{pictures/IntArch}\\
    \caption{Integral Architecture}\label{fig:IntArch}
  \end{minipage}
\end{figure}
 And  in general, operations
management/management science research has treated modularity as a
means to increase commonality across different product variants
within a product family, i.e. to allow for the same component(s) to
be used in multiple product variants, and when feasible, in all
product variants.\\ In addition, research in operations management/
management science has highlighted that modularity in product design
may allow for the design of a loosely coupled production system in
which different subassemblies can be made independently, and then
rapidly assembled together in different ways, given technical
constraints, to build the final product configurations.
\cite{Salvador2002}


Modularity can take a number of forms and shapes. A visual
representation is given in figure \ref{fig:modular}, stack
modularity is shown in figure \ref{fig:sta}



\begin{figure}[t]
  \centering
  \includegraphics[width=\textwidth]{pictures/modular}\\
  \caption{Six type of modularity for the Mass customisation of Products and Services \cite{Pine1993}}\label{fig:modular}
\end{figure}
\begin{figure}[t]
  \centering
   \includegraphics[width=.3\textwidth]{pictures/stack}\\
     \caption{Stack Modularity \cite{Miller1999}}\label{fig:sta}
\end{figure}




\subsection{Stack Modularity} A collection of modules that may be
connected to create a unit with a value in some dimension that is
the sum of the individual modules. \\Examples: Battery power supply,
multi stage pumps, aircraft industry.

\subsection{Component-Sharing Modularity}
As the name says components are shared between products to provide
economies of scope. This   is often done to standardise components
and allows a low-cost  production of a large variety of products
and/or services. Component-sharing modularity is best used to reduce
the number of parts and thereby the costs of an existing product
line that already has high     variety.\\Example: A standard power
supply used for more products.

\subsection{Component-Swapping Modularity}
This is the complement of     the Component-Sharing Modularity.
Here, different components are paired with the same basic product,
creating as many products as there are components to swap. In many
cases, the distinction between component sharing and component
swapping is a matter of degree. For a company providing a
standardised product or services today, the key to taking advantage
of component-swapping modularity is to find the most customisable
part of the product or service and separate it into a component that
can easily be reintegrated. For greatest effectiveness, the
separated component should have three    characteristics:


\begin{enumerate}
        \item  It should provide high value to the customer
        \item  Once separated, it should be easily and seamlessly reintegrate
        \item   It should have great variety to meet differing customer needs and wants
\end{enumerate}

True individual customisation comes when there are an infinite
number of components to be swapped.
\\Example: Notebook computers with different pointing
devices, disk drives and keyboards.

\subsection{ Cut-to-Fit Modularity}
This technique (\cite{Du2001} calls it fabricate-to-fit) is similar
to the previous two types, except that in cut-to-fit modularity one
or more of the components is continually variable within preset or
practical limits. Cut-to-fit modularity is most useful for products
whose customer value rests greatly on a component that can be
continually varied to match individual wants and needs. If the
current product line has components that step up discontinuously in
size then competitive advantage can be gained by mass customising
the products to fit individuals, eliminating the compromises
customers must other wise make.\\Example: adjusting the flow
characteristic of a hydraulic valve by turning grooves in the main
spool. Electric wires.

\subsection{Mix Modularity}
This type of modularity can use any of the above types, with the
clear distinction that the components are so mixed together that
they themselves become something different. The key factor in
determining if you can take advantage of mix modularity is recipe.
Anything with a recipe can be varied for different markets,
different locals, and indeed for different individuals. To reach
perfect customisation requires that you move from processing your
recipe according to a predetermined plan to a process-to-order
operation, and then economically reduce the batch size to one.
Ideally, create a process that moves the last step out to the
customer for instant, point-of-delivery customisation.

\subsection{   Bus Modularity}
This type of modularity uses a standard structure that can attach a
number of different kinds of components. The term comes from
computers and other electronic equipment that use a bus, or
backplane, that forms the primary pathway of information transfer
between processing units, memory, disk drives, and other components
that can plug into the bus. The key to using bus modularity is of
course the existence of a bus. If your product or services has a
definite standard but changeable structure, think about breaking it
up by, first, defining the product architecture or services
infrastructure that is really required for each customer, and
second, modularising everything else into the components that can be
plugged into that standard     structure.\\Examples: Conveyor
systems, electric supply network.

\subsection{  Sectional Modularity}

Sectional modularity allows the configuration of any number of
different types of component in arbitrary ways - as long as each
component is connected to another at standard interface. The classic
example is Lego building blocks with their locking cylinder
interface. The number of objects that can be built with Lego is
limited only by the imagination. With sectional modularity, the
structure or architecture of the product itself ca change, providing
tremendous possibilities for variety and customisation. Sectional
modularity is the most robust of the six types but it is also the
most difficult to achieve. The key is to develop and interface that
allows sections or objects of different types to interlock.  It may
be much easier sectional modularity in services. Many service
companies achieve mass customisation through the creation of
low-level sectional modules - authors call these components
micro-units - that can be mixed in a variety of combinations to
match localised or individual customer needs. In both products and
services the ability to mass customise through sectional modularity
provides the most robust capabilities for mass customisation.
\\Example: Kitchen furniture, Lego blocks. \cite{Pine1993}



Modularity helps the product variety and to shorten the
time-to-market it can also help to create economies of scope. So
when implementing modularity one can even extend the number of
different product variation while decreasing the manufacturing cost.
Hence for a successful implementation of mass customisation
modularity is imperative.\cite{Duray2000}\\

Modularity enables producers to be more flexible in the choice of
end product and facilitates consumer involvement at different stages
in the production process, also known as postponement. This concept
of consumer involvement is explained in paragraph \ref{sec:custInv}.

%modfigs zijn figuren van modularity
%\input{modfigs}


\subsection{Drawback of modularity}\label{sec:drawback}
When using a modular product structure there are some obstacles in
the proverbial road. Only when the disadvantages of a modular design
of that specific part way up to the advantages of the found in other
area's, the modular design should be utilised.

\begin{description}
    \item[Cost \& Complexity] The modular design does add some additional
cost to the program. Here mainly the extra fittings and
interconnections are to blame.
    \item[Design Difficulties] In the design stage of the product
    family the designer have to have an good understanding of the
    inner workings of the whole product or process. Because they
    have to develop visible design rules to make the modules
    function as a whole.\\ Also possible problem only arise when
    those specific modules are put together. This could be long
    after design of the initial product.
    \item[Excessive number of variants] When using a modular design
    the number of possible variants can explode. \cite{Sheikh2003}
    gives an example where he uses a farm tractor. With only eleven
    basic functional choices in where each chose has two variants and occasionally three  such as: Fuel, horsepower
    transmission etc. the total number of configurations accumulate
    to $2^8*3^3$= 6912 possible variants. Adding one more modular
    feature, say a SatNav integrated in the roof, adds an extra 6912
    possible configurations

\end{description}



\section{Customer involvement}\label{sec:custInv}

Product families, modularity, DFMC is all meant to give the
potential customer the possibility to get involved in the process of
configuring his product, while keeping the production cost within
limits.

Thus customer involvement is essential for the mass customisation.
The key factor is \emph{postponement} that is, differentiating a
product for a specific customer until the latest possible point in
the supply network. By adopting such a  approach, companies can
operate at maximum efficiency and quickly meet customer�s orders
with a minimum amount of inventory. \cite{Fietzinger1997}

 The level
of customer involvement in the value chain plays a critical role in
determining the degree of uniqueness of the product and the type of
customisation. By extension, the point of customer involvement in
the production cycle is a key indicator of the degree or type of
customisation provided. If customers are involved in the early
design stages of the production cycle, a product could be highly
customised. If customer preferences are included only at the final
assembly stages, the degree of customisation will be not as great.
In this manner, point of customer involvement provides a practical
indicator of the relative degree of product customisation
\cite{Duray2002} as seen in the customisation continuum of
\cite{Lampel1996}.




\subsection{Manufacturing environment strategies}\label{sec:MES}

At this point it is necessary to introduce four manufacturing
environment strategies based on the level of direct customer
involvement in production planning and scheduling. This typology can
be referred to as product-positioning strategies \cite{Sheikh2003}.

The four strategies are listed below. Where from 1 to 4 there is a
trend from standardisation to customisation.
 \begin{enumerate}
    \item Make to Stock (MTS)
    \item Assemble to Order (ATO)
    \item Make to Order (MTO)
    \item Engineer to order (ETO)
\end{enumerate}




\subsubsection{Make to Stock (MTS)}

One can connect this principle with the \emph{pure standardisation}
in the customisation continuum of \cite{Lampel1996}. The products
are fabricated exactly the same and put in a buffer. A nice
historical example of this principle are the T-Ford produces by
Henri Ford. They were available in all colors as long as they were
black. These are also called off-the-shelf products. They have a
good quality and are reasonably prised. The product has a very short
delivery time, but the inventory costs are relatively high.

\subsubsection{Assemble to Order (ATO)}

When a customer has several options, as with computers and
automobiles, standard components can be assembles to form a final
product. The customer can chose to have a 100 GB hard disk or a 200
GB hard disk. Both are available and can be connected to the
motherboard. The same holds for the CPU and other components. A
prerequisite here is a modular (see \ref{sec:mod}) setup of the
components, they need to be compatible. In the customisation
continuum of \cite{Lampel1996} this corresponds to \emph{Customised
Standardisation}. Although the delivery time is slightly larger than
in the MTS case, the  inventory costs are lower and the customer
satisfaction will be higher due to a more tailored product.

\subsubsection{Make to Order (MTO)}

Usually the production of the product does not start until the order
arrives at the factory. The product is comprised  of several
predefined (and already designed) parts. Occasionally a custom
designed part can be incorporated in the final product. \\ The MTO
is used with quite  expensive, relative small orders. The customer
is generally willing to wait for the order to be produced. Some
examples are tailed-made clothing, machinery and products made to
customer specifications, this is called \emph{Tailored
Customisation} in the customisation continuum. The delivery time is
quite long and also the costs are quite high compared to a ATO
product. Only the inventory costs of any raw material are relevant
to the product cost.

\subsubsection{Engineer to order (ETO)}

This is \emph{pure customisation} according to the customisation
continuum. The parts and products have never before been made by
this specific company. Everything has to de designed and tested. A
high customer involvement is demanded and involves high costs.
Generally these products are one in a kind and very expansive. The
whole process (lead time) takes very long naturally the inventory
cost are nonexistent

\subsection{Order entry points}\label{sec:COEP}

It is obvious these manufacturing strategies are related to the
point where the customer gets involved in the production process. In
other words where the order enters the production process. This
brings up a the concept of the  order entry point (OEP).
\cite{Dekkers2000} \cite{Bikker1998}

 The point when an order does start order-oriented
activities on a process is known as the Order Entry Point. Before
this point work in progress is directed on prognosis. We have to
recognise that in every main process corresponding with the primary
process; an Order Entry Point may be introduced, dividing the
process in a part that is organised and controlled on prognosis and
in a second part that is controlled on order.\\
Two order entry points are distinguished, one for the hardware flow,
the \emph{Custom order entry point} and one for the information
flow, the \emph{Order specification entry point}. In figure
\ref{fig:OEP} the implications of the OEP are visualised.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{pictures/orderentry}\\
  \caption{Different positions of COEP and OSEP in the sales, engineering and manufacturing process \cite{Dekkers2000}}\label{fig:OEP}
\end{figure}

\subsubsection{Custom order entry point}
The point where an order penetrates into hardware flow, the Custom
Order Entry Point (COEP), determines which specific activities have
to be undertaken after the start of custom order as seen in figure
\ref{fig:OEP}.\\ The COEP is also known as the customer order
decoupling point. This point distinguishes between forecast- or
push-driven operations that are run in anticipation of future
customer orders and order- or pull-driven operations that are
run-based upon customer orders. All activities in the supply chain
performed after the CODP are customised and targeted at the specific
customer order, while all activities in the supply chain performed
before the CODP are standardised. The principle obviously holds a
link with the postponement concept, yet there is more to
postponement, such as the (geographical) positioning of operations
close to the end-consumer. \cite{Hoek2001}

\subsubsection{Order specification entry point}

At the same time that companies have to decrease lead-time, they
have to improve productivity as well. As with the production flow
the information flow has a point where the order penetrates into the
process. The position of the OSEP indicates the the amount of
engineering work has to be done on the order. The more design and
configuration work has been done in advance, the less the amount of
engineering remains to be done for processing a specific order. \cite{Dekkers2000}


\subsection{Order entry matrix}

\begin{wrapfigure}{R}{7.5cm}
   \includegraphics[width=.55\textwidth]{pictures/OEM}\\
  \caption{Order entry matrix \cite{Dekkers2000}}\label{fig:OEM}
\end{wrapfigure}

The choice for a specific OSEP point does not necessarily indicate
the use of a specific COEP point or vice versa. The choice of each
point depends on rather independent factors. Since we distinguish
two order entry points, these points might be put together in a
matrix. Different products and constructions may require different
positions of these order entry points in the Order Entry Matrix (see
Figure \ref{fig:OEM}).

The choice of the COEP determines mostly the performance of
manufacturing. The decision for the COEP one derives from:

\begin{itemize}
    \item The availability of the product specification for production
    \item The permitted lead-time
    \item The frequency of expected sales of individual modules
    \item The capital investment in stocks
\end{itemize}



Before this entry point, the prognosis of the orders guides the
planning of manufacturing activities; for example, the production
planning might be based on MRP. After this entry point, the emphasis
on control of manufacturing shifts to lead-time and flexibility
since specific customer requirements have to be met.

The position of the OSEP may vary from dong all design and
engineering work in order to having completed all engineering work
on beforehand.This crucial decision depends on the \emph{calculated
risk} of defining constructions before receiving actual orders, the
prospects of applying the se constructions during the product family
life-cycle and feather benefits. \cite{Dekkers2000}

The OSEP/COEP combinations are manyfold. In table \ref{tab:OEP} The
combinations with regard to the Customisation Continuum and the
Manufacturing Environment Strategies are given.

\input{OEP}
%\input{Doc3}
The COEP specifies the position in the chain where the customisation
occurs. Furthermore, the COEP indicates the extent to which
operations are pull/order-driven by customisation versus
push/forecast-driven by standardisation. It also reveals which
functions are involved in the customisation, reasoning that all
functional activities positioned down-stream of the COEP play a role
in customisation. Postponement combines these specifications,
integrating both principles into one operating
system.\cite{Hoek2001}

\section{Conclusion}


In this chapter the  concept of mass customisation has been
explained. The necessary prerequisites, as there are product
families, modularity, DFMC as well as the different variants in
customisation according to \cite{Lampel1996}. \\ This is the
organisational side of the equation. The responsibilities as far as
the manufacturing and design side of the company are concerned have
been covered. Now the information delivery from customer to company
and visa versa is to be examined.
