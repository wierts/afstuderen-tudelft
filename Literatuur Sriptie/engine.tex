\subsection{Rule-based configurators}


 Rule-based configurators
work by executing rules with the following form: ''if condition
then consequence'' IF \{Conditions for insertion of part X at
position Y\} THEN \{insert part X in position Y\}. The product
solutions are derived in a forward chaining manner. At each step,
the system examines the entire set of rules and considers only the
set of rules that can be executed next. Furthermore, there is no
separation between directed relationships and actions. Thus, rules
contain both the domain knowledge such as compatibilities between
components and the control strategy that is necessary to compute
the solution to a specific configuration problem \cite{Blecker2004}.\\
Ultimately, the result were highly complex rules with strong
interactions and a bloated knowledge base that was extremely hard
to maintain due to the hidden interactions between different
rules. The maintenance efforts required for continued use of the
rule-based expert system family became so large that companies had
to developed a whole methodology for their maintenance. The high
cost associated with the rule-based systems led to  a strong
interest in finding some view of configuration that would satisfy
the actual intent of knowledge representation: a clear model of
the domain (the what) cleanly separated from the inference
strategy (the how) used for finding a solution.
\cite{Stumptner1997}

XCON is a good illustration of the complexity of knowledge
maintenance. In 1989, its knowledge base had more than 31,000
components and approximately 17,500 rules. The change rate of this
knowledge averaged about 40\% per year \cite{Sabin1998}.

\subsection{model-based configurators}

These drawbacks, especially the maintenance problems, let to the
development of new knowledge systems, the model based systems.
This is a group of configurators of which the main consideration
was the existence of a \emph{model}, which consists of
decomposable entities and interactions between their elements.\\
The most important model-based representation types that are
implemented within configurators are: logic-based, resource-based
and constraint-based approaches.The main motivation for using
model based configuration systems lies in the nature of
configuration, which is by definition a synthesis task. Therefore
the ability to cover the entire range of possible solutions
systematically is essential. Although a system can gain experience
in a particular domain and use it to improve efficiency, the
system should still work for application domains in which it has
no prior experience. There are several model-based approaches to
configuration. Here only the three mentioned above will be
explained to more detail

\subsubsection{Logic-based}
Both \cite{Sabin1998} and  \cite{Blecker2004} mention the
logic-based controller. The most prominent family of logic-based
approaches is based on description logic. Description logics are
formalisms for representing and reasoning with knowledge. They are
based on the notions of individuals (objects), concepts (unary
predicates, classes), roles (binary relations) and constructors that
allow complex concepts and roles to be built from atomic ones. The
inference mechanism is based on subsumption. This is the decision
whether the one concept is more general than the other.
\\The advantages of Description Logics (DLs) during the knowledge-
acquisition phase are clear. The classification facility
automatically organises descriptions into an explicit taxonomy,
based on subsumption inferences. In addition, DL systems
automatically maintain consistency as new descriptions are
classified and added to the knowledge base, or as existing
descriptions are modified and reclassified. There is one drawback
however. The trade-off between the efficiency of the reasoning
tasks and the expressiveness of the knowledge-representation tool
is crucial. If the formalism aims at a certain level of
expressiveness (by allowing existential quantifications or
disjunctions, for example), subsumption becomes NP-complete. On
the other hand, restricting expressiveness to ensure tractability
makes the formalism unable to represent complex systems, which is
often necessary in representing practical configuration tasks.



\subsubsection{Resource-based}
\cite{MAILHARRO1998} states that the resource-based paradigm
provides an intuitive producer or consumer model but does not
account well for the topology of the configured artifact. For
example, a rack produces a resource that is its plugging slots. Each
card plugged in this rack consumes one or more slots. This kind of
knowledge is captured well by resource modelling, but topological
constraints as ''cards of type A can only be plugged in slots 1 to
8'' or ''if a card of type A is plugged in slot 1, then two cards of
type B must be plugged in slots 2 and 3'' cannot be represented. In
real-world configuration problems, there are topological constraints
as well as resource balancing constraints. So a framework dedicated
to configuration problems must be able to take into account both of
the constraint types.


As seen, resource-based configurators are based upon a
producer-consumer model of the configuration task. Each technical
entity is characterised by the amount of resources it supplies,
uses and consumes.

The goal is to find a set of components that bring the overall set
of resources to a balanced state, in which all demands are
fulfilled. A configuration is acceptable only if the resources the
environment and different components demand are each balanced by
the resources the environment and components can maximally supply.
The algorithm for solving a configuration task in this context is
straightfonvard.ls Start with the set of resources demanded by the
environment in the requirement specifications. Then, select one
resource type that is not balanced yet and create the list of
component types that can supply that resource. Select one
component type from the list and add an instance of that component
to the current (partial) configuration. Repeat this process until
for every resource the required amount is less than or equal to
the amount supplied by the environment or by components. In case
of a dead end, backtrack to the last choice point.

This approach is well suited for configuration tasks for which the
main concern is covering a desired functionality, especially when
single components only partially satisfy that functionality. For
example, the system offers an elegant solution for the
configuration of modular systems, where all constraints are
resource-based. The method loses its elegance and simplicity when
forced to deal with structural and specific placement
requirements.

\subsubsection{Constraint-based}

In constraint-based reasoning components are defined by a set of
properties and a set of connection ports. Constraints among
components restrict the ways components can be combined. A
restriction can forbid a combination of parts (Part A cannot be
combined with part B) or can require a specific combination (Part
A requires part C) \cite{Blecker2004}. The problem solving method
of constrained-based architecture is based on two assumptions.
First, configuration is usually a purposeful activity-that is, the
user knows ahead of time the kinds of functional roles that the
artifact must fulfill. Second, for each functional role, the user
can identify one or more components as key components for
providing that role. This means that any implementation of that
function will include these components. We can represent this
restricted form of the configuration task as a
constraint-satisfaction problem, in which components and their
ports are the variables and the constraints restrict the way
components can be integrated in a solution.\\ The restriction for
this kind of modelling is the fact that all variables have to be
declared from the beginning.

\subsection{Case-based configurators}



The case-based approach is fundamentally different from rule-based
and model-based approaches, all of which were based on the
assumption of a complete knowledge base which describes general
principles holding for all possible configurations. A configurator
would then use these principles to try and compose new
configurations. Instead, Case-Based Reasoning starts out from a
library of previously solved problems in the domain, searches for
those cases whose requirements correspond best with the current,
unsolved case, and then attempts to use the corresponding
solution, with only those alterations which have to be made due to
differences in the requirements.  The main advantage of the
case-based approach is that the knowledge acquisition process does
not require the formulation of general principles for the whole
domain, but instead re-uses and adapts existing examples. It is,
however, necessary to develop an appropriate similarity metric, a
catalog of possible (meaningful) alteration operations and
strategies for their application, since the fetching of
inappropriate cases, or inappropriate tampering with an almost
correct case can significantly impair the efficiency of the
approach and the quality of the solutions. Above all, the
existence of a sufficiently broad example base is a basic
requirement. Interestingly enough, while case-based design systems
are common research vehicles in domains such as mechanical design
(typically understood to be more difficult than configuration).
\cite{Blecker2004} \cite{Stumptner1997} and \cite{Sabin1998}.
