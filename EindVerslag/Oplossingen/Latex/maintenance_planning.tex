\section{Manufacturing Planning}\label{sec:planning}
As seen in figure \ref{tab:planneddt} a large part of the low
utilisation percentage is due to the lack of orders, or idle time,
for the respective assets. Part of this issue has been resolved by
reducing the number of production shifts from 3 to 2. However one
will never be able to totally eliminate the time no orders are
available for a certain asset. \\By decreasing the idle time one can
increase the availability of the assets. The otherwise not used time
can be used to do scheduled maintenance or cleaning. In using this
idle time more time will be  available for production \footnote{see
figure \ref{fig:times}} thus increasing the
availability.\\
This is easier said than done. In order to synchronise the planning
of  \maint\  and  production of the assets one has to have
information on the following topics for those assets:


\begin{itemize*}
\item Production demand
\begin{itemize*}
\item type of product to be produced
\item amount of product to be produced
\end{itemize*}
\item maintenance requirements
\begin{itemize*}
\item type of maintenance to be performed
\item duration of the maintenance requirement
\item flexibility of the maintenance requirement
\end{itemize*}
\end{itemize*}

Next to planning the operations(\maint\ and \pro) also the personnel
has to have a schedule. If a \maint\ task is planned the
personnel for that tasks needs to be available.\\

These topics will be explained in more detail in the next sections.
Next they will be integrated into the bigger picture to clarify how
they interact to create a workable production and maintenance
schedule, which obeys all policies\footnote{QSD 12.01.01} and
regulations\footnote{see appendix on GMP}.

\subsection{Production Demand}
With the production demand one has information the amount and type
of product to be produced and thus the hours to be reserved for
production. The type of product is also of interest for any
subsequent changeover\footnote{for more information on changeovers
see section \ref{sec:changeovers}}. As one has already the same
information on the preceding product, one can determine the time
needed to change for one product type to an other. With the
information at this stage one can plan the production demand for the
set time frame.

\subsection{Maintenance requirements}

For the maintenance to be planned into the production schedule one
has to know what \maint\ activities are scheduled for that period.
This is  retained in the SAP maintenance program. Every maintenance
activity is specified and has a set interval. SAP automatically
provides a  notification when a \maint\ activity is due.\\ However
no nominal \maint\ tasks times are specified for the \maint\
activities. For the \maint\ activities to be scheduled they have to
have a nominal tasks time that can be used to plan the \maint\ into
the scheduling system.\\

The flexibility of the \maint\ activities are also of importance.
Each \maint\ has a window in which it has to be performed dependant
on the interval. A \maint\ activity is flexible if it is not
critical\footnote{See the critical \maint\ determination in appendix
\ref{app:critical}}. If the \maint\ activity is not critical one can
be flexible in the execution of the \maint\ activity and thus plan
it without having to interrupt the production activities. The
process to determine as to the criticality of a \maint\ activity is
describe in section \ref{sec:critical}.


\subsection{Planning of personnel}
As said, the planning of \maint\ tasks is only possible if the
required personnel is available. This means the personnel has to be
planned in advance to be available to do the \maint. Currently there
is no clearly defined planning for the \maint\ personnel. Apart for
the earlier discussed separation in mechanics for the formulation
and packaging department, and the electricians who serve
both\footnote{see figure \ref{fig:MaintOrg}} no further task
separation has been made. This lack in planning can lead to a lack
of availability of personnel. The lack of (availability) of
personnel has one or more of the following consequences.


\begin{itemize*}
  \item Idling of the system, because no personnel is available to
  do the repair
  \item  The repair or \maint\ tasks are outsourced
  \item As a reaction more personnel is hired and an overcapacity is
  created.
  \item Personnel is called away from original tasks and upon
  returning to those tasks has to reacquaint with the original task
  (personnel changeover times)
\end{itemize*}

To combat these consequences these consequences and the costs
associated with these consequences, the availability of the \maint\
personnel has to be increased. This can be done with the following
measures.\\

By using task separation the personnel changeovers can be reduced
and the availability increased. Task separation means that in a
given shift a predefined number of \maint\ personnel is scheduled to
do that task and that task only. So one group of mechanics (one or
more) is will solely be doing preventive maintenance, while an other
group will do the demand maintenance and a third the changeovers. By
separating the tasks of the personnel the jobs can be planned. With
the planning of the jobs the availability of the personnel can be
increased and the personnel
changeovers can be reduced and the `fire-fighting' can be minimized.\\
This is the first step in the personnel planning. As part of the job
separation and planning, norms have to be set for the different
tasks the personnel has to do. With these norms the personnel to do
the tasks themselves can be planned. The norms for the planned
maintenance tasks are to be defined as part of the maintenance
improvement program\footnote{See section \ref{sec:MIP}} and the
changeover norms should be established as part of the Changeover
Program\footnote{See section \ref{sec:changeovers}}. Norms for other
activities can also be created such as cleaning tasks. With the
norms in place the utilisation of the \maint\ personnel can be
analysed and improved.\\

The utilisation of the \maint\ personnel can be analysed using
Queuing Theory an Little's Theorem. Little's Theorem states that
\begin{equation}
    N= \lambda T
\end{equation}
Here N is the number of tasks in the queue (DM orders), $\lambda$ is
the arrival rate of the tasks and T is the time it takes to service
the task (MTTR). This means the number of tasks is directly
proportional to the MTTR and the arrival rate of the orders.\\ The
order arrival queue can be seen as a M/M/1 queuing
system\footnote{For more information on queuing systems see the book
Factory Physics by Hopp and Spearman}. This indicates the arrival of
the orders and service time is modeled using a poisson process.
There is one server(all orders are positioned in the same queue) and
the orders are services according the \ac{FCFS} principle. Using
this knowledge the mean number of orders can be found by writing
\begin{eqnarray}
% \nonumber to remove numbering (before each equation)
  \rho &=& \frac{\lambda}{\mu} \\
  N &=& \frac{\rho}{1-\rho}
\end{eqnarray}
Where $\mu$ is the capacity of the servers, so the number of people
servicing the order queue.\\
 The predicted behavior of the
utilisation is given for $\rho$ going from 0 to 1 in
\figref{fig:queue}. One can see that if the utilisation is
approaching 85 \% the necessary capacity (number of \maint\
personnel) and throughput time (time spend servicing the assets)
approaches infinity.
\begin{figure}[h]
  \centering
  \includegraphics[width=.66\textwidth]{oplossingen/pictures/queuing/queue}\\
  \caption{Predicted utilisation behavior}\label{fig:queue}
\end{figure}

Therefor the utilisation of the \maint\ personnel should be
optimised for 80 \% to 85 \%. A higher number is not practical.
\subsection{Effect}
The planning of the maintenance tasks with the \pro\ activities can
free up more time for production. Maintenance and cleaning tasks can
be performed while the machine is idle. For the planning to be
possible the  tasks performed by the \maint\ personnel need to have
nominal work times. This determination can be done in conjunction
with the critical maintenance determination for the PM tasks and for the changeovers in the project dedicated to those changeovers. \\
In combination with planning the personnel and separating the
responsibilities within a shift the  result  will be an increase in
time available for production.
