\chapter{Second \maint\ phase}\label{ch:2nd.phase}

With the conditions for the first \maint\ phase defined one can turn
to the second phase. Some elements are already in place but others
have to be implemented. To grow into phase two a continual
improvement is a must. By doing so the \maint\ organisation can
learn and improve using its own data and experience. This will
create a basis for the step toward the third \maint\ phase.\\ The
Maintenance Master plan  and the improvement program work will
describe work already done and that still has to be done.

\section{Maintenance Master Plan}\label{sec:critical}
The Maintenance master plan at \abb\ is a project to determine the
consequences of the tasks the current \ac{PM} tasks have on the
assets in terms of criticality\footnote{Critical is defined as:
Those preventive \maint\ tasks that, if not done (on time) could
have a negative impact on the product quality}. This provides \abb\
with the knowledge to what extend the various \ac{PM} activities
influence the quality of the products and gives the organisation a
means to evaluate the planning of the various \ac{PM} activities.
The determination
has to be made for assets that are \ac{GMP}\footnote{For the complete description of GMP see appendix \ref{app:GMP}} relevant.\\

To make this distinction in criticality, the list of all assets
subject to the \ac{GMP} had to be updated. These include all
production assets, the air treatment facilities and oxygen and
nitrous delivery systems. From this list all maintenance plans per
asset where inventoried. The scope and extent of the \maint\
activities have been researched. With this information prioritising
of the decision process can start. The steps for the decision
process  are described in \figref{fig:critical}.

\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{oplossingen/pictures/masterplan/masterplan}\\
  \caption{Flow diagram for criticality determination}\label{fig:critical}
\end{figure}
\noindent Step 1: The \maint\ lists have to be adapted to facilitate
easy
marking\footnote{see \figref{fig:hokjes}} if an action is critical or not.\\
Step 2: The engineer responsible for the asset marks his
recommendation on the maintenance list and concludes by signing.\\
Step 3: This recommendation is verified by the Quality department
and the User of the asset. They accept the recommendation by signing
the \maint\ list.\\
If either Quality or User does not agree with the engineer they
notify the other parties of their objection and provide a rational
for the different opinion. If no consensus can be reached based on
the rational provided, a meeting is to take place to determine the
the criticality. \\
Step 4: Implement the criticality determination into SAP such that
for each \maint\ order the criticality is listed on the order.\\

The no critical determination of a \maint\ task reduces the
paperwork associated with the \maint\ task. The Abbott global
policies dictated no Exception Reports\footnote{These are documents
to record a \ac{PM} tasks being performed outside the set time
window} have to be written by the  \maint\ crew  when non critical
\maint\ tasks have been performed late. These Exception Reports are
necessary when a critical \maint\ task is performed late. If this
were the case than Exception Reports have to be written which in
turn initiate a quality check for that asset as a result of the late
performing of the \maint\ task. This can generate possible
production interruption. The paperwork also keeps the \maint\ crew
 from doing the job of
maintaining the assets.\\

The distinction in criticality provides HoM with a first overview of
those \maint\ tasks that have possible negative consequences. Thus
have a high risk\footnote{Risk based \maint\ will be explained in a
later section in this chapter} potential. The HoM can take this into
account when planning the \maint\ and \maint\ personnel. He also he
can use this information when prioritising the \maint\ and \pro\
plans into one \manu\ schedule.

\section{Maintenance improvement program}
\begin{wrapfigure}{r}{7cm}
       \includegraphics[width=.5\textwidth]{appendices/figures/6sigma/DMAIC}
       \caption{DMAIC loop}
       \label{fig:DMAIC}
\end{wrapfigure}

The rationale of the program is based on the path of the \ac{DMAIC}
process, which is part of the six sigma\footnote{See appendix
\ref{app:6sigma}} theory. This approach is chosen because of its
clear structure and good applicability in \maint\ optimisation
\cite{Cooper}.

\subsection{Define: Setting targets}
The definition of the improvement program is the basis for the whole
program. All future decisions stem from the actions taken at this
point. For \abb\ this means the Maintenance Improvement program
targets the availability of the assets. Through the decrease of
downtime resulting from breakdowns the availability will be
increased. A 90\% availability will be the final target for \abb.
This can be achieved with a couple of intermitted steps. As a first
 step, the target  of 50\% is reasonable considering the
current state of the availability. \\The \maint\ improvement program
will use the \ac{RBM} theory to identify those tasks with the
highest risk. Risk is defined as:
\begin{equation}\label{fn:risk}
    \text{Risk}= \text{likelihood of occurrence} \times
    \text{consequence}
\end{equation}
\noindent The consequences are divided into two categories:
\ac{SHER} and
Financial.\\

Further targets are the reduction of changeovers and changeover
duration. Although changeovers are not directly related to \maint\
they do influence exhorter. Therefore this topic has to be covered
when advancing to through the second \maint\ phase.

\subsection{Measuring: Data acquisition}
The data necessary to analyse and improve the availability is taken
from two sources. The SAP system provides the financial data and
running time data, while the breakdown data has to be gathered from
the reports the mechanics write after repair.

\subsubsection{SAP data}
The financial data now used to control the \maint\ expenses can also
be used to calculate the \acl{MI}, \acl{SPL} and \ac{OSMP}.  The
calculation can be automated using the SAP \ac{ERP} package.
Reporting the \acp{kpi} can be done each month via the same
system.\\ The \ac{WOC} measuring can be kept the same, however it
should be used by the \ac{hom} to keep track of the order
completion. The other \acp{kpi} (\ac{MTTF}, \ac{MTBF} and \ac{MTTR})
have to be deducted from the repair data.

\subsubsection{Breakdown data}\label{sec:breakdown.data}
In the current \ac{CMMS} at \abb\ a \ac{SBS} for the assets in the
\pro\ department up to component level is present. This enables a
buildup of breakdown database up to component detail. This database
is retained in SAP. The database has to be filled with breakdown
data by the \maint\ personnel. For this purpose a \ac{GUI} has been
developed.

\paragraph{GUI}
Each breakdown has to be described in as much detail as necessary.
Here one has to make a trade-off. On the one hand as much data as
possible is desired to better analyse the situation resulting in the
breakdown. On the other hand requiring very detailed information
takes time to gather. The person having done the repair is also
asked to fill in information regarding the breakdown, keeping him
away from his 'core business'.\\To facilitate the ease of entering
the data in the database and making it accessible for later
analysis, a \ac{GUI} (see \figref{fig:gui}) has been developed,
based on the terminology used by the \maint\ crew. \\

The GUI is set up using the Kepner-Tregoe ``Problem Analysis''
strategy. The K-T strategy has the following method of describing
the problem at hand:
\begin{itemize*}
\item Describe the Problem (Problem
Bezeignung\footnote{German equivalent in the GUI, see
\figref{fig:gui}\label{fn:gui}})
\item Describe the problem
\begin{itemize*}
\item What (Bezugsobject\footref{fn:gui})
\item Where (Bezugsobject\footref{fn:gui})
\item When (Störungsdaten\footref{fn:gui})
\item Extend (Position\footref{fn:gui})
\end{itemize*}
\end{itemize*}

When stating the problem the object (asset that has the breakdown)
and the defect (description of the problem) should be given in. Next
the asset itself and the component, which has the defect, should be
described.\\
The time to repair is entered (form the start and end times the
total will be calculated). This is thus the time the asset was
unavailable. Followed by the extend of the problem (breakdown) has
to be described. This is done by selecting the different
possibilities from dropdown menus in the three fields listed below:
\begin{itemize*} \item Auswirkung (implication) \item Schadenbild
(Failure Description)
\item Ursache (Cause)
\end{itemize*}

\begin{figure}[h]
 \centering
  \includegraphics[width=.95\textwidth]{oplossingen/pictures/gui/maske}\\
  \caption{\acl{GUI}}\label{fig:gui}
\end{figure}

\subsubsection{Usage of the information}
The information from the repairs is bundled in a database. The
breakdown database provides the information for the MTBF, MTTR. This
database also provides the information for the Pareto analysis as
defined in the next section. The other KPIs can be calculated using
the data available through the financial module in SAP. The exact
calculation methods of the various KPIs is explained in appendix
\ref{app:KPI}.

%%%%%%%nieuw stuk
\subsection{Failure Analysis}
With the creation of the database with asset failure data one can
commence with analysing the failures. The analyses is done using the
following techniques in the order described below.

\begin{enumerate*}
  \item Pareto Analysis
  \item \acl{FMEA}
  \item Consequence Description
  \end{enumerate*}


\subsubsection{Pareto Analysis}

The Pareto\footnote{Named after Vilfredo Pareto 1848-1923} analysis
is based on the Pareto principle\footnote{ This is also known as the
80-20 rule, the law of the vital few and the principle of factor
sparsity} assuming 80\% of the failures are caused by 20\% assets.
This is visualised by creating a Pareto chart as shown in
\figref{fig:pareto}.
\begin{figure}[h]
 \centering
    \includegraphics[width=.85\textwidth]{oplossingen/pictures/pareto/pareto_theorie}\\
  \caption{Pareto chart}\label{fig:pareto}
\end{figure}
\noindent The SAP system is programmed to automatically generate a
Pareto graph from  the failure database inputs. This pinpoints the
assets in the formulation, meltrex and packaging department that are
subject to high failure percentages and thus need to be examined.\\

Secondly one can zoom-in and do the same for the components per
asset, thus looking at asset level as opposed to (sub) department
level.\\ With the Pareto analysis the top 20\% components (or
assets) are identified that cause the most breakdowns. This reduces
the number of components under investigation and focusses on items
which are contributing to the downtime of the asset. To understand
the breakdowns better \abb\ has to perform a
\ac{FMEA} on these top 20 \% assets.\\




\subsubsection{FMEA}\label{sec:fmea}

By  doing a \ac{FMEA}\footnote{Sometimes FMECA (failure modes and
criticality analysis)is used for a FMEA. See appendix \ref{app:FMEA}
for a description on how to perform a FMEA and the difference
between FMEA and FMECA. Throughout this report FMEA will be used}
the effect a failure has on the production process is being
identified.  Also doing a \ac{FMEA}  is a laborious and most often
costly exercise.  Now only the failure effects of the parts that
cause the most downtime are taken into consideration. \\
\begin{figure}[h]
  \centering
  \includegraphics[width=.6\textwidth]{oplossingen/pictures/extruder/extruder}\\
  \caption{Example of a extruder feeding screw}\label{fig:extruder.screw}
\end{figure}
This \ac{FMEA} results in an easy and clarifying overview of the
asset, components and their function, failure modes and effects of
those failures. For Abbott this has been done for the Extruder ZSK
70\footnote{This extruder is part of the Kaletra project} shown in
\figref{fig:extruder}\footnote{The format is developed by the FMEA
teams at Chrysler, Ford, and General Motors}. So all information
regarding the modes and effects of the failure possibilities can be
presented.  The individual steps of the FMEA are described in
appendix
\ref{app:FMEA}\\

In establishing the failure modes and effects one can determine what
happens if a component fails and how it fails. With the FMEA there
is a detailed and logical description of the cause and effect of the
failures. With these effects the consequences can be described. This
is done in the next section.
\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{oplossingen/pictures/FMEA/fmea_extruder}\\
  \caption{FMEA example for extruder ZSK 70}\label{fig:extruder}
\end{figure}

\paragraph{\ac{RCA}} At this point \acl{RCA} has to be mentioned.
RCA implies that if one drills far enough, it is possible to arrive
at a final level of causation. However a great many failures are due
to human error \cite{Mobray1997}. More often than not it is
impossible to arrive at a final level of causation and the root
finding will go on for ever.
\begin{quote}
For instance: say a failure mode is 'impeller nut over tightened'.
This is caused by an 'assembly error' (level n+1), for the 'fitter
was distracted' (level n+2). The reason being 'his child was ill'
(level n+3). This is because 'child ate bad food' (level N+4).
\end{quote} Clearly this can go on and on. Hence it would reach far beyond the
point desired by the organization\footnote{Human error is a subject
on it's own. More on Human error can be found in: Human error by
James Reason, 1990}. Under normal circumstances one should acquire
adequate information with the three level analysis as defined in
paragraph \emph{\nameref{par:3level}}. Should the three levels not
provide any information than a deeper investigation can be
considered.

\subsubsection{Current failure reasons}
The reasons for breakdowns at \abb\ can be grouped as follows:
\begin{itemize*}
\item maintenance and repairs
\item using/setup failures (human errors)
\item wear and tear (asset aging)
\end{itemize*}
To come to this grouping an investigation has been done from the
log-books and the production reports at the formulation and
packaging department. This is combined with the existing grouping of
failures used at \abb. \\For now this grouping will be used as the
basis for the improvement. With the analysis described above a
better and more detailed failure grouping can be done.


\subsubsection{Failure consequences}
In the improvement program the focus is on the consequences of the
failures. The difference between consequence and effect is: A
failure effect answers the question \textbf{what happened?}, where
the failure consequence the \textbf{(how) does that matter?}
question answers. This has also been  mentioned in appendix \ref{app:FMEA}.\\
The effects (of the failures) have been identified in the FMEA. Thus
now the next step in the analysis phase is, describing the
consequences of the failures. This is an important step as here the
foundation for the improvement phase later on\footnote{This is also
one of the important philosophies of the \ac{RBM} theory. The focus
on the consequences of the failure as to the effect of the failure}.
By answering the following questions the consequences can be made
evident and aide in the \maint\ task evaluation and
definition. The questions are:\\
\noindent


\begin{itemize*}
\item Is the failure evident\\
Does the failure have an influence on
\item Safety, Health and/or Environment?
\item Product Quality?
\item What is the cost of failure and its consequential damage to the
product?
\item Does the failure mode have an influence on operational
performance?
\end{itemize*}
\noindent With the answers to these questions one can determine the
consequences of the failures. These consequences can be written down
on a dedicated form as seen in \figref{fig:diagram}.\\

\begin{figure}
  \centering
  \includegraphics[width=\textwidth]{oplossingen/pictures/FMEA/maint_diagram}\\
  \caption{Failure consequence diagram example for extruder ZSK 70}\label{fig:diagram}
\end{figure}

The consequences an their related failures have to be seen in
conjunction with the likelihood this consequence will occur. If the
likelihood of the extruder screw breaking is extremely low, what use
has focussing on the problem in the first place. The combination of
likelihood and consequence is discussed in the next section.

\subsubsection{Risk Matrix}
From the FMEA the likelihood of failures is known. With the answers
to the above questions the consequences of failures are known. Both
likelihood and consequences can be given a factor\footnote{See
appendix \ref{app:risk.matrix}}. The risk is than computed and this
is visualised in \figref{fig:risk.matrix}.
\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{oplossingen/pictures/risk.matrix/risk_matrix-3}\\
  \caption{Risk Matrix \footnotesize{Adapted form Prof. Smit 2005}}\label{fig:risk.matrix}
\end{figure}

By organising the consequences and the likelihood in this
fashion\footnote{An other method is calculating the \acl{RPN} this
method is briefly discussed in appendix \ref{app:risk.matrix}} one
can
identify immediately which combination poses the greatest threat.\\
Obviously components in the red area (the six top left corner
blocks) are not acceptable as they are now. Should component be
situated in this red area because of \ac{SHE} consequences a
redesign of the situation is very much to be considered.\\

The process of determining the risk of  failure consequences on the
level of product quality is being initiated within Abbott. This is
to be done as part of the `Maintenance Master Plan'. It creates an
opportunity to focus on those \maint\ tasks that are vital for the
product quality and form a basis for the identification of those
areas that poses the highest risk. The failure likelihood is
determined using the MTTF or MTBF depending on the component. When
deemed necessary a similar determination on the basis of cost can be
established.


\subsubsection{Conclusions}
By presenting the component failure data (gathered form the repair
processes) in a Pareto chart, one is able to focus on the few
components that cause the most failures (80-20 rule). These 'few'
components (and there assets) are then evaluated via a FMEA. The
'Pareto selection' is done out of financial and time considerations.
FMEA is time consuming and thus expensive.\\Presenting the
components according to risk of failure consequence and likelihood
those components that pose the highest risk are identified. These
are the components that have to be dealt with immediately. The
current knowledge about the failure groups can be improved using the
analysis.


%%%%%%%nieuw stuk


\subsection{Improvement}
The improvements must  act on three categories of failures
identified earlier.
\begin{itemize*}
\item maintenance and repairs
\item usage and setup failures (human errors)
\item wear and tear (asset aging)
\end{itemize*}

\subsubsection{Maintenance and related improvements}
For the components whose \ac{PM} strategy must be changed, based on
the analysis done the possible
 options are:
\begin{itemize*}
\item Run to failure
\item Preventive maintenance
\item Predictive maintenance\footnote{The concept of \ac{CM} will be
explained in more detail and to more extend in \ref{sec:cm}}
\end{itemize*}
Their relation is given in \figref{fig:maint.division}.

\begin{figure}[h]
  \centering
  \includegraphics[width=.66\textwidth]{oplossingen/pictures/maintenance/maintenance}\\
  \caption{Maintenance Breakdown}\label{fig:maint.division}
\end{figure}

The decision, which of these possibilities is best used for the
components under investigation, is based on three criteria. With
these criteria in mind the engineer can make the decisions in what
direction to go with the \maint\ of the part under investigation.
The criteria are:
\begin{description}
  \item[Applicability] The applicability is to be seen in relation to the
consequences of the failure. It is applicable if it can eliminate or
avoid the failure, or at least reduce the probability of the
likelihood to an acceptable level.\\With the extensive research done
in the FMEA and consequence listings all information is available to
make the determination if a certain task is applicable.
  \item[Cost effectiveness] The task(s) selected to prevent the
failure should not cost more than the failure (and its extend) it is
intended to prevent.
  \item[Technical Feasibility] This is mainly the case for
  predictive \maint\ (\acl{CM}). It may not always be possible to
  implement CM for a certain component or asset, because the
  techniques aren't available or one can not monitor the behavior.
\end{description}


An overall increase of \maint\ intervals to reduce the downtime is
not recommended. The utilisation of the assets at \abb\ is around 25
\%. This has it's effect on the economics of \maint. Increasing the
amount of \ac{PM} will not cut the \maint\ and \maint\ related
costs.\\
\begin{figure}[h]
 \centering
  \includegraphics[width=.8\textwidth]{Oplossingen/Pictures/cmpm/optim}\\
  \caption{Optimising Preventive maintenance source: prof. K. Smit}\label{fig:maint.optim}
\end{figure}
Applying \figref{fig:maint.optim} to the situation at \abb\ gives
the following situation. The total \maint\ costs are at the moment
3.78M \euro\footnote{see \appref{app:KPI}}. This is achieved with a
ratio of PM 25\% and DM 75\%. According to graph \ref{fig:min.cost}
the PM/DM ratio with the lowest total cost is between 30\% and 40\%
This will add up to an estimated 3,7M \euro\ in annual \maint\ cost
and around 80k \euro\ in, mostly, personnel cost reduction annually.

%The behaviour of the demand \maint\ cost as the \ac{PM} increases is
%$y=\frac{1}{\sqrt{x}}$ and the \ac{PM} cost behaviour is $y=4\times
%x$\\




\begin{figure}[h]
  \centering
  \includegraphics[width=\textwidth]{oplossingen/pictures/cmpm/figuur}\\
  \caption{minimum \maint\ cost }\label{fig:min.cost}
\end{figure}
The determination to change the interval or PM strategy has to be
made per component or asset. To support in this determination a
decision logic plan has been created for the \maint\ task selection.

\begin{figure}[h]
  % Requires \usepackage{graphicx}
  \includegraphics[width=.95\textwidth]{oplossingen/pictures/besluitenboom/besluitenboom}\\
  \caption{Decision Logic Diagram}\label{fig:decision.Tree}
\end{figure}
The sacrifices (costs if one were able to measure this) of failures
with SHE consequences are irrelevant. Therefor the decision on the
\maint\ task for these failures, is based solely on the results
achieved with the tasks.\\As stated earlier when the failure of
components has consequences that are situated in the red area of the
Risk Matrix\footnote{see \figref{fig:risk.matrix}} a redesign is to
be considered apart from the possibilities the \maint\ has. In the
decision logic diagram this is not taken into account and the
redesign option is only then suggested 'when all else fails'.\\
Failures with no SHE consequences  have \maint\ tasks based on the
cost effectiveness, applicability and feasibility of the \maint\
task. This means that the tasks are evaluated based on what the task
brings to the table and the subsequent cost that are associated with
this solution.\\

For those components identified in the risk matrix with a \ac{PM}
strategy the intervals have to be reduced. The \ac{MTBF} and
\ac{MTTF} have to be used to correct the interval. \\The exact
interval determination is an art in itself. It can not only be based
statistical data but the assets itself also play a role in this
determination. Therefor it is a task that the engineers responsible
for the assets have to make. They must do this in cooperation with
the asset user and the \acl{hom}. The engineers are the ones that
are close to the asset and thus have a `feeling' for the asset and
can give practical information on that particular asset. This
process must be done with all components that are identified in the
risk matrix and need
a PM program.\\
For other assets a interval extension can be in order. Here the MTBF
and MTTF can be used to extend the intervals used. Also the
knowledge from the 3 shift \pro\ plan could aide in extending the
intervals. This has not been further investigated.


The extruder screws mentioned earlier are vital to the production of
the extruder. Currently no \ac{CM} techniques are available to
monitor the state of the screw. Thus a \ac{PM} program is necessary
for the screw. This \ac{PM} program is already in place for the
extruder screw. Should the screw still be suffering from failures as
described in \secref{sec:fmea}. The interval has to be changed. The
MTBF is one of the \acp{kpi} to monitor the behaviour of the screw
failures. Using the MTBF one can reset the \maint\ interval to
prevent the screw from breaking down.


\subsubsection{Usage and setup failures}
The human error\footnote{Human error is a subject on it's own. In
\cite{Mobray1997} and  in ``Human Error'' by James Reason, 1990,
more on human error can be found} factor has not been researched
into detail. Using the knowledge on MTBF and MTTF from the part
manufacturer and comparing this to the data at \abb\ possible human
error failures can be eliminated.\\ Failures due to setup errors
should be seen as part of the changeover process. They originate
during the changeovers. Therefor the changeover procedures have to
be analysed and improved to find and eliminate these setup failures.
Possible problems in this area can be monitored using the MTBF and
MTTF.\\

To improve the situation with usage and setup failures the \ac{hom}
can initiate solutions that involve training to use or changeover
certain assets. Revise the  protocol in using or setting up certain
assets. Or engineer solutions to prevent a failure from happening
again.
\subsubsection{Wear and tear}
Aging is a natural phenomena that effects all assets. The aging
behaviour of general systems is given in \figref{fig:bathtub}. The
point where the aging process starts depends on the asset. The point
when to replace the asset can be identified using \acp{kpi} defined
in section \ref{sec:asset.level}. The repair or buy decision can be
made using the \ac{MI} KPI. This is the responsibility of the
\acl{hom}.\\

The aging process can be accelerated due to dust, filth and other
contaminations. In 2005 the \ac{PEP} program is initiated, and as a
part of that program the 5s principle has been adopted. The 5s
principles deal with a clean working environment based on the
Japanese concept of house keeping. The five words are described here
along with the equivalent English words and the English meaning.

\begin{description}
  \item[Seiri] (Sort) Put things in order
(remove what is not needed and keep what is needed)
  \item[Seiton] (Straighten) Proper
Arrangement (Place things in such a way that they can be easily
reached whenever they are needed)
  \item[Seiso] (Shine) Clean (Keep things clean
and polished; no trash or dirt in the workplace)
  \item[Seiketsu] (Standardise) Purity (Maintain cleanliness after cleaning
perpetual cleaning)
  \item[Shitsuke] (Sustain) Commitment (Actually this is not a
part of '4S', but a typical teaching and attitude towards any
undertaking to inspire pride and adherence to standards established
for the four components)
\end{description}
As this project is being implemented at present, the effects are not
yet obvious. Also the writer has had no part in this program.
Therefor no further investigation into this possibility has been
done and no results can be produced.\\
\begin{figure}
 \centering
  \includegraphics[width=.4\textwidth]{oplossingen/pictures/5s/5s}\\
  \caption{5S in practice at \abb}\label{fig:5s}
\end{figure}
Apart from preventing breakdowns due to dust and filth, a clean
working environment also permits the use of the human senses to
detect possible failures. The senses are look, listen, feel and
smell. Due to the cleanliness of the working environment changes in
the environment are easily detected. In fact this is a low level
form of condition monitoring\footnote{For more information on
condition monitoring see section \ref{sec:cm}}





\subsection{Controlling}
The controlling phase is a look back and look forward phase at the
same time. Looking back, the previous four process steps have to be
evaluated. All stakeholders have to be involved in the evaluation of
the first cycle. This is done to ensure  that the gains made in the
project are not lost over a period of time.\\

As every one knows 'old habits dy hard'. The engineers responsible
for the asset have to make sure the 'old ways' are not creeping back
into the working process. The \acl{hom} has to control the
improvement process. On the other hand the people doing the \maint\
have to know the new processes. For this reason the working
practices of the \maint\ crew are to be examined and updated to the
new situation. This has been done by updating the \ac{SOP}.\\
The changes in the SOP include, implementation of the critical
preventive \maint\ execution. The description and execution of
\acl{DM} and the use of the repair data retention in SAP. A further
overall evaluation of the document has been made to assure the SOP
is conform the Global
Abbott Policies. The SOP is shown in appendix SOP.\\

Controlling the cycle is also initiating the following cycle. With
the evaluation of the gains of the first cycle the direction of the
next cycle can be identified. In this last step the program can be
steered into the direction that than deserves the most attention.
This is a continues improvement that has than benefits when it is
repeated a number of times. The \ac{hom} has the oversight to close
the last cycle and initiate a new one with all the corresponding
actions.

\section{Changeovers}\label{sec:changeovers} Changeovers amount to
quite a large portion of the downtimes of the assets. For the OEE to
increase it is important to decrease the changeover time. The more
these changeovers are done by the \maint\ personnel. The workload of
the \maint\ personnel is very high at the moment\footnote{See
figures in appendix \ref{app:resourcen}}. The changeovers are the
result of the produce to order strategy currently employed by
Abbott. This does not mean no improvement can
be made in this area.\\
The classic approach to changeovers is seen in
\figref{fig:changeover}

\begin{figure}[h]
  % Requires \usepackage{graphicx}
  \includegraphics[width=.95\textwidth]{oplossingen/pictures/smed/smed}\\
  \caption{Classic Changeovers}\label{fig:changeover}
\end{figure}

When the last part from the production  batch is processed, the
machine is stopped and changeover operations all happen. The machine
will start again after completion of trials. Often no standard
method is defined, procedures and checklists do not exist and there
is no
teamwork, meaning several operators sharing these operations.\\

Think of the moment you get a flat tire (and you do not have the new
'run-flat' tires). The process of changing the tire will take about
15 minutes. If you were to go to the local Kwikfit they may be able
to do this in a bit under 10 minutes. Even better if you were to
drive in F1, you could have 4 new tires in less than 5 seconds.\\

The example shows that with the proper tools and preparation and
training (Kwikfit) one can make an 33\% improvement in the
changeover. However when the design, synchronisation, strategy and
timing we can improve even more(F1).\\

\acl{SMED}\footnote{See appendix \ref{app:SMED} on the SMED theory.}
is a method of systematic seeking for setup time reduction,
according to a quantified target.\\ This improvement possibility has
also been identified by Abbott and a project to tackle the long and
frequent changeovers has been set-up. The project will start in the
beginning of 2006. The writer did not take part in this project as
it is a project initiated by the \pro\ department. So no data as of
current is available about the effects and possible results.


The power of the SMED theory to reduce the changeover times at
Abbott has already been identified by \cite{koning2000}. This study
has been done for the packaging department with an estimated 40 \%
reduction in changeover time. At present the formulation department
can also benefit from the reduction in changeovers.

\section{Possible results}
The financial result of reducing the downtimes through the reduction
of breakdowns is found in decrease of personnel hours and the
increase in potential production time. The cost of waste product due
to breakdowns is negligible compared to the other costs
associated with the breakdown.\\
With the new \maint\ program it is the author's estimation the
following improvements can be achieved.
\begin{itemize*}
    \item All \acl{PM} can be done outside  production
    hours. Thus increasing the availability by an average of 30 \% points
    \item The changeovers can be reduced by 40 \%
    \item The breakdowns can be reduced with the aide of the
    Maintenance improvement program by 30--40 \%
\end{itemize*}
The increase in availability can be achieved by planning the
maintenance tasks so they do not interrupt the production
activities. The reduction of changeover and breakdown times gives a
total of 30\% points from 40\% (average) to 70\% (average)
availability.\\

The changeover reduction will have the largest net reduction
potential. Table \ref{tab:imp.change} shows the potential hours
saved by improving the changeovers is around 6000.
\input{oplossingen/latex/changeover}
The potential savings in \maint\ hours are shown in
\tabref{tab:imp.maint} around 1600 hours.\\
\input{oplossingen/latex/PM}

%In \tabref{tab:gain} the increase in running time is calculated if
%the availability is increased with 1\%. The increase is calculated
%as a global increase and does not favor any one area of downtime.
%\input{oplossingen/latex/plus1}
%An one per cent increase equals an increase in 304 hours of
%production over a 55 day period. This amounts to 1382 hours annually
%for the same one per cent increase in availability. \\

Based on the work load in the measuring period, the extra production
time generated by the downtime reduction can also be used to
compress the production to one shift instead of the current 2
shifts. An other possibility is to reduce the working week from 6 to
5 days. For this to be possible proper planning of both the \maint\
and production is required. Apart from the PM activities to be
performed outside the \pro\ schedule,  the capacity of the assets
has to be planned to be used equally. Thus maximising the production
potential.\\ These estimation have been made without considering
improvements in production speed and product quality. Any
improvements on these parts  have an additional positive influence
on the production capacity. The effect of these improvements are
seen in increases in OEE and TEEP.
