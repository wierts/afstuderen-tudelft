\chapter{\ac{rcm}}
``\ac{rcm} is a maintenance perspective in an operational context-
understanding plant goals, needs and equipment(how it  servers ages
and fails) and then developing a \maint\ strategy to optimize
outcomes in the context of the goals.''\\


an approach to maintenance that combines reactive, preventive,
predictive, and proactive maintenance practices and strategies to
maximize the life that a piece of equipment functions in the
required manner %rcm guide

\section{History}

\acf{rcm} has its origins in the aircraft industry of the late 1960s
and early 1970s.Then it was realized that aircraft maintenance
philosophies of scheduled overhauls were based on flawed
paradigms.\\
\begin{wrapfigure}{l}{8cm}
  \includegraphics[width=.5\textwidth]{appendices/figures/baring/baring}\\
  \caption{Baring life scatter}\label{fig:baring}
\end{wrapfigure}
Extensive research into aircraft component failure characteristics
in the 1950s, 60s and 70s has shown that the intuitive belief that
failure could be attributed to ``wear out'' type failures was wrong.
As an example see \figref{fig:baring} and  in fact the majority of
components displayed failure characteristics that indicated that
failure was most likely to occur when the component was new or just
after maintenance. This ``discovery'', combined with reality that
complex wide bodied aircraft such as the Boeing 747 could not be
economically maintained through scheduled overhauls, forced a new
approach to determining aircraft
scheduled maintenance requirements.\\


\ac{rcm} was first described by Stan Nowlan and Howard Heap in 1978.
\acl{rcm}  set out the definitive approach to determining
aircraft maintenance requirements.\\

Industries outside aviation were slow to realize that the basic
philosophies embodied within Nowlan and Heap's RCM report could
equally be applied to assets other than aircraft. (An aircraft is,
after all, only a highly specialized and refined machine.)
Consequently, RCM did not come into use in other industries until
the mid 1980s, and has only become popular in the 1990s, some 20
years after its basic
philosophies were embraced in the aviation industry.\\

The adoption of RCM by other industries has not been without its
hiccups and setbacks. Not all industries have the culture of
thoroughness, rigour, analytical skills, and project management
know-how of the aviation industry. As a result, industries seeking
minimum effort, inexpensive, ``quick fix'' solutions have been
disappointed in their attempts to apply RCM. Where it has been
applied properly, RCM has achieved outstanding benefits and results.
Consequently, RCM has
received ``mixed reviews'' in its application in general industry.\\


\section{What it does}

RCM is emphasizing four principles \cite{Tweeddale2003}:

\begin{enumerate}
  \item The purpose is to minimize or prevent the consequence of
  breakdowns, not the breakdowns themselves. The emphasis should be
  on prevention or reducing the frequency of those breakdowns that
  have the more serious consequences.
  \item Consequences of failures depend on where the asset is
  located and operating. Two different organizations with
  similar assets have entirely different maintenance programs.
  \item It is not assumed all failures can be prevented nor is this
  desired.
  \item The focus is on the the capability required form the
  asset, its function, not on the asset it self.
\end{enumerate}
