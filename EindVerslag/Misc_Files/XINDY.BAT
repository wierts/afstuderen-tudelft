@echo off

REM test procedure for xindyOS2 under MSDOS
REM usage: xindy.bat xdy-file raw-file ind-file

REM Adjust all paths to your needs!

SET RSX=c:\XINDYDOS\EMTEX\BIN\RSX.EXE
SET EMX=c:\XINDYDOS\EMX\BIN\EMX.EXE

echo (progn                                                 > tmp.lsp
echo (searchpath ".;f:/xindydos/modules")        >> tmp.lsp
echo (xindy:startup                                       >> tmp.lsp
echo :idxstyle "%1"                                       >> tmp.lsp
echo :rawindex "%2"                                     >> tmp.lsp
echo :output "%3")                                       >> tmp.lsp
echo (exit)))                                               >> tmp.lsp

c:\xindydos\bin\xindy.exe -q -M c:\xindydos\bin\xindy.mem tmp.lsp

SET RSX=
SET EMX=
